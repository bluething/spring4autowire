package com.pegipegi;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pegipegi.javaconfig.KnightConfig;
import com.pegipegi.knights.Knight;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
  		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("ConstructorInjection.xml");
  		Knight knight = context.getBean(Knight.class);
  		knight.embarkOnQuest();
  		
  		context.close();
  		
      AnnotationConfigApplicationContext context2 = new AnnotationConfigApplicationContext(KnightConfig.class);
      Knight knight2 = context2.getBean(Knight.class);
      knight2.embarkOnQuest();
      context2.close();
    }
}
