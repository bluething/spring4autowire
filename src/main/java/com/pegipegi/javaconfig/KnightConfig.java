/**
 * 
 */
package com.pegipegi.javaconfig;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
@Configuration
@ComponentScan(basePackages={"com.pegipegi"})
public class KnightConfig {}
