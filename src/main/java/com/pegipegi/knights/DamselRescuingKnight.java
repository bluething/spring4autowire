/**
 * 
 */
package com.pegipegi.knights;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pegipegi.quest.Quest;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
@Component
public class DamselRescuingKnight implements Knight {
	
	public DamselRescuingKnight(){}
	
	@Autowired
	private Quest quest;
	
	/**
	 * 
	 */
	public DamselRescuingKnight(Quest quest) {
		this.quest = quest;
	}

	/* (non-Javadoc)
	 * @see com.pegipegi.knights.Knight#embarkOnQuest()
	 */
	@Override
	public void embarkOnQuest() {
		quest.embark();
	}

	public void setQuest(Quest quest) {
		this.quest = quest;
	}

}
