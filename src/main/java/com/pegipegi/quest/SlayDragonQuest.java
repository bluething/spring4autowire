/**
 * 
 */
package com.pegipegi.quest;

import java.io.PrintStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
@Component
public class SlayDragonQuest implements Quest {
	
	public SlayDragonQuest(){}
	
/*	@Autowired
	private PrintStream stream;*/
	
/*	public SlayDragonQuest(PrintStream stream) {
		this.stream = stream;
	}*/

	/* (non-Javadoc)
	 * @see com.pegipegi.quest.Quest#embark()
	 */
	@Override
	public void embark() {
		System.out.println("Embarking on quest to slay the dragon!");
	}

/*	public void setStream(PrintStream stream) {
		this.stream = stream;
	}*/

}
